import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-numeros',
  templateUrl: './numeros.component.html',
  styleUrls: ['./numeros.component.css']
})
export class NumerosComponent {

  public ventasNetas: number = 7200000.5512;
  public porcentaje: number = 0.4856;

}

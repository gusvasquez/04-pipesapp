import { Component, OnInit } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
import { interval } from 'rxjs';

@Component({
  selector: 'app-no-comunes',
  templateUrl: './no-comunes.component.html',
  styleUrls: ['./no-comunes.component.css']
})
export class NoComunesComponent implements OnInit {

  //i18nSelect
  public nombre: string = 'Gustavo';
  public genero: string = 'masculino';
  public invitacion = {
    'masculino': 'invitarlo',
    'femenino': 'invitarla'
  };
  //i18nPlural
  public clientes: string[] = ['Andres', 'Camilo', 'Pedro', 'Javier'];
  public clientesMap = {
    '=0': 'no tenemos ningun cliente esperando',
    '=1': 'tenemos 1 cliente esperando',
    '=2': 'tenemos 2 clientes esperando',
    'other': 'tenemos # clientes esperando',
  };
  constructor(private primengConfig: PrimeNGConfig) { }

  ngOnInit(): void {
    this.primengConfig.ripple = true;
  }

  cambiarnombre() {
    this.nombre = 'Angie';
    this.genero = 'femenino';
    console.log(this.nombre);
  }

  quitarDatos() {
    this.clientes.pop();
  }

  // Keyvalue Pipe
  public persona = {
    nombre: 'Gustavo',
    edad: 27,
    direccion: 'Neiva, Huila'
  }

  // jsonPipe
  public heroes = [
    {
      nombre: 'Superman',
      vuela: true
    },
    {
      nombre: 'Spiderman',
      vuela: false
    }
  ];
  // async pipe
  miObserbale = interval(1000);
  public valorPromesa = new Promise((resolve, reject)=>{
    setTimeout(() => {
      resolve('Tenemos la data de la promesa');
    }, 2500);
  });

}

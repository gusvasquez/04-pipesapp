import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-basicos',
  templateUrl: './basicos.component.html',
  styleUrls: ['./basicos.component.css']
})
export class BasicosComponent implements OnInit {

  public nombreLower: string = 'Andres';
  public nombreUpper: string = 'ANDRES';
  public nombreCompleto: string = 'AnDrEs VaSquEz'

  public fecha: Date = new Date(); // el dia de hoy

  constructor() { }

  ngOnInit(): void {
  }

}

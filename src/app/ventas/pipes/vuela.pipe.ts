import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'vuela'
})
export class VuelaPipe implements PipeTransform{
  transform(value: boolean): string {
    //if(value === true){
    //  return 'Vuela';
    //}else{
    //  return 'No Vuela';
    //}
    // tambien se puede hacer de la siguiente forma y hace lo mismo que con los if
    return (value)
    ? 'vuela' : 'no vuela';
  }

}

import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  // es obligatorio poner el name porque es nombre que va a tener el pipe
  name: 'mayusculas'
})
// todos los pipes deben tener implementado el metodo PipeTransform
export class MayusculasPipe implements PipeTransform{

  // se debe devolver un valor el transform o si no sirve

  // para poner un argumento con un valor
  transform(value: string, argumento: boolean = true):string {
    if(argumento === true){
      return value.toUpperCase();
    }else{
      return value.toLocaleLowerCase();
    }

  }

}

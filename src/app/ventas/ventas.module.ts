import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// components
import { NumerosComponent } from './pages/numeros/numeros.component';
import { NoComunesComponent } from './pages/no-comunes/no-comunes.component';
import { BasicosComponent } from './pages/basicos/basicos.component';
import { OrdenarComponent } from './pages/ordenar/ordenar.component';
// primeNG
import { PrimeNgModule } from '../prime-ng/prime-ng.module';
// se importa el pipe que se va a manejar y se pone en las declaraciones
import { MayusculasPipe } from './pipes/mayusculas.pipe';
import { VuelaPipe } from './pipes/vuela.pipe';
import { OrdenarPipe } from './pipes/ordenar.pipe';

@NgModule({
  declarations: [
    NumerosComponent,
    NoComunesComponent,
    BasicosComponent,
    OrdenarComponent,
    MayusculasPipe,
    VuelaPipe,
    OrdenarPipe
  ],
  imports: [
    CommonModule,
    // importamos el primeNG porque lo vamos a utilizar
    PrimeNgModule
  ],
  // se deben exportar las modulos ya que se van a utlizar fuera de la carpeta y del modulo de ventas
  exports: [
    NumerosComponent,
    NoComunesComponent,
    BasicosComponent,
    OrdenarComponent
  ]
})
export class VentasModule { }

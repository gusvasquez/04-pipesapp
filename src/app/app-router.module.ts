import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BasicosComponent } from './ventas/pages/basicos/basicos.component';
import { NumerosComponent } from './ventas/pages/numeros/numeros.component';
import { NoComunesComponent } from './ventas/pages/no-comunes/no-comunes.component';
import { OrdenarComponent } from './ventas/pages/ordenar/ordenar.component';

// se crea la routes de tipo Rorutes y se crean las rutas para cada componente
const routes: Routes = [
  {path: '', component: BasicosComponent, pathMatch: 'full'},
  {path: 'numeros', component: NumerosComponent},
  {path: 'no-comunes', component: NoComunesComponent},
  {path: 'ordenar', component: OrdenarComponent},
  // si no encuenta ninguna ruta entonces se redirecciona al ruta '' que seria del componente BasicosComponent
  {path: '**', redirectTo:''}
];

@NgModule({
  imports: [
    // se importa RouterModule y se pone el metodo forRoot porque son las rutas principales
    // se crea una variable const que no cambia de valor y se pone en el forRoot que es donde van las rutas principales
    RouterModule.forRoot(routes),

  ], exports: [
    // se exporta el modulo RouteModule para utilizar el router-outlet en app.component.html para renderice la ruta a la cual ingresamos
    RouterModule
  ]
})
export class AppRouterModule { }

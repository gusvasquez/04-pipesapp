import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './menu/menu.component';

// modulo donde estan los imports de PrimeNg
import { PrimeNgModule } from '../prime-ng/prime-ng.module';

@NgModule({
  declarations: [
    MenuComponent
  ],
  imports: [
    CommonModule,
    PrimeNgModule
  ],
  // se va utlizar fuera del modulo el componente Menu entonces tenemos que exportarlo
  exports:[
    MenuComponent
  ]
})
export class SharedModule { }

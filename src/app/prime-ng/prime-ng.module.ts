import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// primeNg
import {ButtonModule} from 'primeng/button';
import {CardModule} from 'primeng/card';
import {MenubarModule} from 'primeng/menubar';
import {FieldsetModule} from 'primeng/fieldset';
import {BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ToolbarModule} from 'primeng/toolbar';
import {TableModule} from 'primeng/table';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
  ],
  // se pone export porque no se va a utlizar en este modulo porque no tenemos componentes
  // cuando se ponen en import es porque se van a utilizar en componentes que esten el modulo
  exports:[
    ButtonModule,
    CardModule,
    MenubarModule,
    FieldsetModule,
    BrowserAnimationsModule,
    ToolbarModule,
    TableModule,
  ]
})
export class PrimeNgModule { }
